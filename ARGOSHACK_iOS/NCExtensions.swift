//
//  NCExtensions.swift
//  BBP
//
//  Created by Aaron De Santiago on 8/12/16.
//  Copyright © 2016 Aaron De Santiago. All rights reserved.
//

import UIKit

class NCExtensions: NSObject {

}

extension UIImage{

    func encode_image(image:UIImage) -> String
    {
        let media_data = UIImagePNGRepresentation(image)
        let base64_string = media_data?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64_string!
    }

}

extension UIViewController {
    

    func keyboardWillShow(notification:NSNotification, constraint:NSLayoutConstraint, constant:CGFloat, positive:Bool)
    {
        var sign_variable : CGFloat = 1
        if !positive { sign_variable = -1 }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            constraint.constant = sign_variable*(keyboardSize.height+constant)
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    func keyboardWillHide(notification:NSNotification, constraint:NSLayoutConstraint)
    {
        constraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
