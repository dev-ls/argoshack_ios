//
//  OBJ_Conceptos.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_Conceptos: NSObject {

    var monto: Int
    var descripcion: String
    var categoria: OBJ_Categoria
    
    init(Categoria:OBJ_Categoria,Descripcion:String,Monto:Int) {
        self.categoria = Categoria
        self.descripcion = Descripcion
        self.monto = Monto
    }

    
}
