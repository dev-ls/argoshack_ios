//
//  VC_Registro_Proyecto_Descripcion.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/6/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_Registro_Proyecto_Descripcion: UIViewController {

    // Outlets
    
    @IBOutlet weak var txt_projectName: NCTextField!
    @IBOutlet weak var txt_projectDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func action_nextsStepProject(_ sender: AnyObject) {
        OBJ_Project.sharedInstance.string_projectName = txt_projectName.text!
        OBJ_Project.sharedInstance.string_projectDescription = txt_projectDescription.text
        
        let vc_next = self.storyboard?.instantiateViewController(withIdentifier: "VC_Registro_Proyecto") as! VC_Registro_Proyecto
        self.navigationController?.pushViewController(vc_next, animated: true)
    }
    
    @IBAction func action_back(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
