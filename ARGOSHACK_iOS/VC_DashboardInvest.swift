//
//  VC_DashboardInvest.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_DashboardInvest: UIViewController, UITextFieldDelegate {
    
    // Previous controller variables
    
    var obj_project = OBJ_ProjectFromList()
    
    // Outlets

    @IBOutlet weak var image_imageMain: UIImageView!
    @IBOutlet weak var lbl_projectName: UILabel!
    @IBOutlet weak var lbl_projectAuthor: UILabel!
    @IBOutlet weak var lbl_amount: NCButton!
    @IBOutlet weak var constraint_percentageCompleted: NSLayoutConstraint!
    @IBOutlet weak var lbl_percentageCompleted: UILabel!
    @IBOutlet weak var lbl_term: UILabel!
    @IBOutlet weak var lbl_interestRate: UILabel!
    @IBOutlet weak var lbl_return: UILabel!
    @IBOutlet weak var lbl_onePayment: UILabel!
    @IBOutlet weak var constraint_bottom: NSLayoutConstraint!
    
    @IBOutlet weak var field_investAmount: NCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        field_investAmount.delegate = self
        
        if let url = NSURL(string: (obj_project.url_image)) {
            if let data = NSData(contentsOf: url as URL) {
                image_imageMain.image = UIImage(data: data as Data)
            }
        }
        
        lbl_projectName.text = obj_project.string_projectName
        lbl_projectAuthor.text = obj_project.string_projectAuthor
        lbl_amount.setTitle(NCFormatters.numberToCurrency(obj_project.float_projectAmount), for: .normal)
        
        lbl_percentageCompleted.text = obj_project.int_projectCompletePercentage.description + "% Completado"
        
        constraint_percentageCompleted.constant = CGFloat(Float(self.view.frame.width - 16) * Float(obj_project.int_projectCompletePercentage)/100.0)
        self.view.layoutIfNeeded()
        
        lbl_term.text = obj_project.int_term.description + " Meses"
        lbl_interestRate.text = obj_project.int_interestRate.description + "%"
        if obj_project.bool_oneInstallment
        {
            lbl_onePayment.text = "Un pago al final"
        }
        else
        {
            lbl_onePayment.text = ""
        }
        
        lbl_return.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Keyboard methods
    
    func keyboardWillShow(notification:NSNotification)
    {
        self.keyboardWillShow(notification: notification, constraint: constraint_bottom, constant: 0, positive: true)
    }
    func keyboardWillHide(notification:NSNotification)
    {
        self.keyboardWillHide(notification: notification, constraint: constraint_bottom)
    }
    
    @IBAction func action_back(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
