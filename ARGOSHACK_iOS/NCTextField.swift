//
//  NCTextField.swift
//  FTP
//
//  Created by Aaron De Santiago on 3/15/16.
//  Copyright © 2016 Aaron De Santiago. All rights reserved.
//

import UIKit

/** Setup del delegate **/

protocol NCTextFieldDelegate:class
{
    func NCTestFieldController(controller:NCTextField, willChangeTextField tag:Int)
}

/** Casos comunes de expresion regular **/

public enum NCTextFielType : Int {
    case Email
    case Name
    case Number
}

class NCTextField: UITextField, UITextFieldDelegate {
    
    /** Delegate **/
    
    var delegateNC : NCTextFieldDelegate?
    
    /** Elementos visuales del campo **/
    
    private var view_back = UIView()
    private var view_error = UIView()
    private var view_focus = UIView()
    
    private var imgView = UIImageView()
    
    //Variables para establecer valores por Storyboard
    
    @IBInspectable var image: UIImage? {
        didSet { imgView.image = image }
    }
    
    @IBInspectable var title: String? {
        didSet { string_title = title! }
    }
    
    @IBInspectable var capacity: Int = 100
    @IBInspectable var formattingPattern : String = ""
    @IBInspectable var showBackground : Bool = true
    @IBInspectable var showIndependent : Bool = false
    
    /** Variables de control del campo **/
    
    var string_regex : String = ""
    var string_errorMessage = "Error"
    var string_title : String = "Default"
    var bool_validInput = false
    var int_lenght = 0
    
    /** Se dibuja el TextField estandar **/
    
    var padding = UIEdgeInsets(top: 18, left: 35, bottom: 0, right: 5)
    
    override func draw(_ rect: CGRect) {
        self.delegate = self
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        
        if showIndependent
        {
            padding = UIEdgeInsets(top: 18, left: 15, bottom: 0, right: 10)
        }
        
        if showBackground
        {
            self.tintColor = UIColor.black
            self.textColor = UIColor.black
            
            self.font = GlobalVariables.appFont
            self.adjustsFontSizeToFitWidth = true
            self.minimumFontSize = 9
            
            //Setup de la vista del fondo
            
            view_back = UIView(frame: self.bounds)
            view_back.layer.cornerRadius = 15
            view_back.clipsToBounds = true
            view_back.isUserInteractionEnabled = false
            view_back.backgroundColor = GlobalVariables.field_back_translucent
            view_back.layer.borderColor = UIColor.gray.cgColor
            view_back.layer.borderWidth = 1.0
            self.addSubview(view_back)
            
            //Setup del icono
            
            imgView.frame = CGRect(x: 8, y: self.frame.height/2 - 10, width: 20, height: 20)
            self.addSubview(imgView)
            
            //Setup del label de titulo
            let labelTitle = UILabel(frame: CGRect(x: 12, y: 2, width: self.bounds.width - 12, height: 20))
            labelTitle.text = string_title
            labelTitle.textColor = GlobalVariables.color_darkGreen
            labelTitle.font = GlobalVariables.appFontTitle
            self.addSubview(labelTitle)
            
            //Setup del view del separador
            // Uncomment for a separator view 
            /*
            let view_separator = UIView(frame: CGRect(x: 0, y: self.frame.height - 1.5, width: self.frame.width, height: 1.5))
            view_separator.isUserInteractionEnabled = false
            view_separator.backgroundColor = UIColor.white
            self.addSubview(view_separator)
            */
            
            //Setup del view de error
            
            view_error = UIView(frame: self.bounds)
            view_error.isUserInteractionEnabled = false
            view_error.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
            view_error.isHidden = true
            self.addSubview(view_error)
            
            //Setup del view de enfoque
            // Uncomment for the setup of the focus view
            /*
            view_focus = UIView(frame: CGRect(x: 0, y: self.frame.height - 2.5, width: self.frame.width, height: 2.5))
            view_focus.isUserInteractionEnabled = false
            view_focus.backgroundColor = accentColor
            self.addSubview(view_focus)
            view_focus.alpha = 0.0
            */
            
            // Setup of the placeholder
            
            if let placeholder = self.placeholder
            {
                self.attributedPlaceholder = NSAttributedString(string:placeholder, attributes:[NSForegroundColorAttributeName: GlobalVariables.field_placeholder])
            }
        }

        if false
        {
            //Cambio del tono del placeholder
            
            if let placeholder = self.placeholder
            {
                self.attributedPlaceholder = NSAttributedString(string:placeholder, attributes:[NSForegroundColorAttributeName: UIColor.white])
            }
            
            //Setup del view del separador especial
            
            let view_separator = UIView(frame: CGRect(x: 0, y: self.frame.height - 1.5, width: self.frame.width, height: 1.5))
            view_separator.isUserInteractionEnabled = false
            view_separator.backgroundColor = UIColor.white
            view_separator.alpha = 0.3
            self.addSubview(view_separator)
        }
        
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if showBackground { return self.newBounds(bounds: bounds) }
        return bounds
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if showBackground { return self.newBounds(bounds: bounds) }
        return bounds
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if showBackground { return self.newBounds(bounds: bounds) }
        return bounds
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= padding.top + padding.bottom
        newBounds.size.width -= padding.left + padding.right
        return newBounds
    }
    
    /** Set de una expresion regular comun **/
    
    func setFieldType(Type:NCTextFielType)
    {
        switch Type.rawValue
        {
        case 0:
            string_regex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            string_errorMessage = "Invalid email"
            break
        case 1:
            string_regex = "[a-zA-Z ]*"
            string_errorMessage = "Invalid name"
            break
        case 2:
            string_regex = "[0-9]*"
            string_errorMessage = "Invalid number"
            break
        default:
            break
        }
    }
    
    /** Detectar actualizacions del text field **/
    //Se revisa cada cambio en el texto para poder mostrarle al usuario si lo que está escribiendo es correcto de acuerdo al contenido que el campo espera recibir. Se obtiene el texto actual más nuevo y se envia al método que se encarga de revisar el texto con la expresión regular registrada en el campo.
    //Se revisa tambien cada cambio en el texto para poder identificar que tan largo es el texto y de esa forma comparar con el limite permitido, en caso de que este sea superado se busca continuar con el siguiente campo, en caso de haber uno.
    //Se incluye el formato del texto en caso de haber sido especificado
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = self.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        
        if capacity != 100
        {
            let string_textState = check_lenght(input: txtAfterUpdate as String)
            if string_textState == "bigger"
            {
                if check_validInput(input: self.text! as String) { sendNotification_lenght() }
            }
            else if string_textState == "same"
            {
                self.text = txtAfterUpdate as String
                if formattingPattern != "" { formatString() }
                if check_validInput(input: self.text! as String) { sendNotification_lenght() }
            }
            else
            {
                self.text = txtAfterUpdate as String
                if formattingPattern != "" { formatString() }
                _ = check_validInput(input: self.text! as String)
            }
        }
        else
        {
            self.text = txtAfterUpdate as String
            if formattingPattern != "" { formatString() }
            _ = check_validInput(input: self.text! as String)
        }
        
        return false
    }
    
    //Lógica de si se muestra o no un mensaje de error con respecto al input actual del campo.
    
    private func check_validInput(input:String)->Bool
    {
        if check_regex(input: input)
        {
            bool_validInput = true
            removeVisualError()
        }
        else
        {
            bool_validInput = false
            setVisualError()
        }
        return bool_validInput
    }
    
    //Revisión por medio de expresiones regulares.
    
    private func check_regex(input:String)->Bool
    {
        if string_regex == ""
        {
            return true
        }
        
        //Preparacion del string
        let textFieldText = input
        do
        {
            let setRegex = try NSRegularExpression(pattern: string_regex, options: .caseInsensitive)
            let matches = setRegex.matches(in: textFieldText, options: [], range: NSMakeRange(0, textFieldText.characters.count))
            
            if matches.count == 1 { return true }
            else { return false }
        }
        catch{}
        return false
    }
    
    //Revision del tamaño del texto
    
    private func check_lenght(input:String)->String
    {
        let int_inputLength = input.characters.count
        int_lenght = capacity
        if int_lenght == int_inputLength
        {
            return "same"
        }
        else if int_lenght < int_inputLength
        {
            return "bigger"
        }
        else
        {
            return "smaller"
        }
    }
    private func sendNotification_lenght()
    {
        //Se utiliza un delegate para poder se escuchada desde cualquier otra clase
        if let delegate = self.delegateNC {
            delegate.NCTestFieldController(controller: self, willChangeTextField: self.tag)
        }
    }
    
    //Formato del texto
    
    func formatString()
    {
        let char_replacement: Character = "*"
        if (self.text?.characters.count)! > 0 && formattingPattern.characters.count > 0 {
            //Se limpia el texto de caracteres utilizados para la separacion, es decir que no sean numeros ni letras
            let string_noFormat = text!.components(separatedBy: NSCharacterSet.alphanumerics.inverted).joined(separator: "")
            
            var string_final = ""
            var stop = false
            
            var index_formatter = formattingPattern.startIndex
            var index_temp = string_noFormat.startIndex
            
            while !stop {
                
                let range_formattingPatternRange = Range(index_formatter..<formattingPattern.index(after: index_formatter))
                
                //Se revisa primero si el character es una variable de formato
                if formattingPattern.substring(with: range_formattingPatternRange) != String(char_replacement)
                {
                    //Cuando el caracter en la guia de formato no es un caracter para ser reemplazado se añade automáticamente.
                    string_final.append(formattingPattern.substring(with: range_formattingPatternRange))
                }
                else if string_noFormat.characters.count > 0
                {
                    //Cuando si es un caracter de reemplazo se toma el siguiente caracter de la cadena sin formato y se agrega a la cadena final.
                    let range_noFormat = Range(index_temp..<string_noFormat.index(after: index_temp))
                    string_final = string_final.appendingFormat(string_noFormat.substring(with: range_noFormat))
                    index_temp = string_noFormat.index(after: index_temp)
                }
                index_formatter = formattingPattern.index(after: index_formatter)
                
                if index_formatter >= formattingPattern.endIndex || index_temp >= string_noFormat.endIndex {
                    stop = true
                }
            }
            text = string_final
        }
    }
    
    /** Detectar cuando se selecciona o deselecciona el campo **/
    //Se revisa cuando se inicia la edición en el campo para mostrar el foco en dicho campo y también cuando se sale para poder quitar el efecto visual. 
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setFocus()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        removeFocus()
    }
    
    /** Establecimiento visual de error en text field **/
    //Se preparan los elementos que se van a mostrar en caso de haber un error en la entrada de texto en el campo.
    
    private func setVisualError()
    {
        view_error.isHidden = false
    }
    private func removeVisualError()
    {
        view_error.isHidden = true
    }
    
    /** Establecimiento visual de foco en text field **/
    //Se preparan los elementos que se van a mostrar en caso de haber seleccionado este campo.
    
    private func setFocus()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.view_focus.alpha = 1.0
            self.view_back.backgroundColor = GlobalVariables.field_back_active
        })
    }
    
    private func removeFocus()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.view_focus.alpha = 0.0
            self.view_back.backgroundColor = GlobalVariables.field_back_translucent
        })
    }
    
}
