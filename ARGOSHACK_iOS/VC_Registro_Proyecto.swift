//
//  VC_Registro.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreLocation

class VC_Registro_Proyecto: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,VC_ConceptDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    
    var a_conceptos = [OBJ_Conceptos]()
    let locationManager = CLLocationManager()
    
    
    // Outlets
    @IBOutlet weak var txt_historyLog: NCTextField!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageView_Granja: UIImageView!
    @IBOutlet weak var txt_fechalimite: NCTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txt_concesionTittle: NCTextField!
    @IBOutlet weak var txt_crop: NCTextField!
    
    var cropPicker = UIPickerView()
    
    // Variables of location
    
    var float_x_waterhole = 0.0
    var float_y_waterhole = 0.0
    
    var float_x_farm = 0.0
    var float_y_farm = 0.0

    var newPhoto: Bool?
    
    // API Data
    
    var array_crops : [OBJ_Categoria] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        print(OBJ_Project.sharedInstance.string_projectName)
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        txt_fechalimite.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(VC_Registro_Proyecto.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        txt_historyLog.text = "Completado"
     
        
        // Setup of picker view
        
        cropPicker.dataSource = self
        cropPicker.delegate = self
        txt_crop.inputView = cropPicker
        
        array_crops = HTTP_Requests.get_ListCropTypes()
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //
    //TableView
    //
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return a_conceptos.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < a_conceptos.count{
            let concepto = a_conceptos[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier:"Cell_Concept", for: indexPath) as! TC_Concepto
            cell.config_Cell(categoria: concepto.categoria.nombre, descripcion: concepto.descripcion, monto: "$" + concepto.monto.description)
            return cell
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_Concept_Add", for: indexPath)
            return cell
        }
    }
    
    
    //
    //Concept Delegate
    //
    
    func append_Concept(concepto:OBJ_Conceptos){
        a_conceptos.append(concepto)
        tableView.reloadData()
    }
    
    // Picker
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return array_crops.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array_crops[row].nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txt_crop.text = array_crops[row].nombre
        let cropid = array_crops[row].id
        OBJ_Project.sharedInstance.id_cropType = Int((cropid as NSString).intValue)
    }

    
    @IBAction func TUI_AddPhoto(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType =
                UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [(kUTTypeImage as NSString) as String]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true,
                         completion: nil)
            newPhoto = true
            
            let locValue:CLLocationCoordinate2D = locationManager.location!.coordinate
            
            //float_x_waterhole = locValue.longitude
            //float_y_waterhole = locValue.latitude
            print("locations = \(locValue.latitude) \(locValue.longitude)")
        }
    }

    
    /*func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }*/
    
    
    //
    //METODOS ESPECIFICOS DEL DATE PICKER
    //
    
    func datePickerValueChanged(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        txt_fechalimite.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func TUI_AddConcept(_ sender: Any) {
        resignFirstResponder()
        let PUVC_Concept = self.storyboard?.instantiateViewController(withIdentifier: "VC_Concept") as! VC_Concept
        PUVC_Concept.delegate = self
        self.addChildViewController(PUVC_Concept)
        PUVC_Concept.view.frame = self.view.bounds
        self.view.addSubview(PUVC_Concept.view)
        PUVC_Concept.didMove(toParentViewController: self)
    }
    
    @IBAction func action_addLogs(_ sender: AnyObject) {
        resignFirstResponder()
        let PUVC_Concept = self.storyboard?.instantiateViewController(withIdentifier: "VC_Historial") as! VC_Historial
        self.addChildViewController(PUVC_Concept)
        PUVC_Concept.view.frame = self.view.bounds
        self.view.addSubview(PUVC_Concept.view)
        PUVC_Concept.didMove(toParentViewController: self)
        
        txt_historyLog.text = "Completado"
    }
    
    // Navigation
    
    @IBAction func action_lastProject(_ sender: AnyObject) {
        OBJ_Project.sharedInstance.image_hole = imageView.image
        OBJ_Project.sharedInstance.image_farm = imageView_Granja.image
        
        //OBJ_Project.sharedInstance.float_x_hole = Float(float_x_waterhole)
        //OBJ_Project.sharedInstance.float_y_hole = Float(float_y_waterhole)
        //OBJ_Project.sharedInstance.float_x_farm = Float(float_x_farm)
        //OBJ_Project.sharedInstance.float_y_farm = Float(float_y_farm)
        
        OBJ_Project.sharedInstance.string_concession = txt_concesionTittle.text!
        OBJ_Project.sharedInstance.string_limitDate = txt_fechalimite.text!
        
        OBJ_Project.sharedInstance.array_projectCategory = a_conceptos
        
        let vc_next = self.storyboard?.instantiateViewController(withIdentifier: "VC_Registro_Proyecto_Amount") as! VC_Registro_Proyecto_Amount
        self.navigationController?.pushViewController(vc_next, animated: true)
    }
    
    @IBAction func action_back(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
