//
//  OBJ_Project.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_Project: NSObject {
    static let sharedInstance = OBJ_Project()
    private override init() {}
    
    // General project
    
    var string_projectName : String = ""
    var string_projectDescription : String = ""
    
    // Images and coordinates
    
    var image_hole : UIImage?
    var image_farm : UIImage?
    
    var float_y_hole : Float = 48.87146
    var float_x_hole : Float = 2.355
    var float_y_farm : Float = 48.87146
    var float_x_farm : Float = 2.355
    
    // Official data
    
    var string_concession : String = ""
    var string_limitDate : String = ""
    var id_cropType : Int = 0
    
    var array_harvestLog : [OBJ_HIstorico] = []
    var array_projectCategory : [OBJ_Conceptos] = []
    
    // Money data
    
    var float_totalAmount : Float = 0.0
    var int_term : Int = 0
    var int_interestRate : Int = 0
    var bool_oneInsallment : Bool = false
    
    
    func resetObject()
    {
        // General project
        
        string_projectName = ""
        string_projectDescription = ""
        
        // Images and coordinates
        
        image_hole = nil
        image_farm = nil
        
        float_y_hole = 48.87146
        float_x_hole = 2.355
        float_y_farm = 48.87146
        float_x_farm = 2.355
        
        // Official data
        
        string_concession = ""
        string_limitDate = ""
        id_cropType = 0
        
        array_harvestLog = []
        array_projectCategory = []
        
        // Money data
        
        float_totalAmount = 0.0
        int_term = 0
        int_interestRate = 0
        bool_oneInsallment = false
    }
    
    func calculateTotalAmount()
    {
        float_totalAmount = 0.0
        for concept in array_projectCategory
        {
            float_totalAmount += Float(concept.monto)
        }
    }
}
