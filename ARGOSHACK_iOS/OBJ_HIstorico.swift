//
//  OBJ_HIstorico.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_HIstorico: NSObject {

    var date:NSDate
    var stringdate: String
    var cultivo: String
    
    init(Date:NSDate,Stringdate: String,Cultivo: String) {
        self.date = Date
        self.stringdate = Stringdate
        self.cultivo = Cultivo
    }
    
}
