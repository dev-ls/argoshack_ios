//
//  VC_Registro_Proyecto_Amount.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/6/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_Registro_Proyecto_Amount: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    
    // Outlets
    
    @IBOutlet weak var txt_amount: UITextField!
    @IBOutlet weak var txt_term: NCTextField!
    @IBOutlet weak var txt_interest: NCTextField!
    @IBOutlet weak var switch_onePayment: UISwitch!
    
    // Data arrays
    
    let a_plazos = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    var plazoPicker = UIPickerView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup of picker view
        
        plazoPicker.dataSource = self
        plazoPicker.delegate = self
        txt_term.inputView = plazoPicker
        
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Picker
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return a_plazos.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return a_plazos[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txt_term.text = a_plazos[row]
    }
    
    @IBAction func action_postProject(_ sender: AnyObject) {
        OBJ_Project.sharedInstance.float_totalAmount = (txt_amount.text! as NSString).floatValue
        OBJ_Project.sharedInstance.int_term = Int((txt_term.text! as NSString).intValue)
        OBJ_Project.sharedInstance.int_interestRate = Int((txt_interest.text! as NSString).intValue)
        OBJ_Project.sharedInstance.bool_oneInsallment = switch_onePayment.isOn
        
        let string_title = OBJ_Project.sharedInstance.string_projectName
        let string_description = OBJ_Project.sharedInstance.string_projectDescription
        let string_water = OBJ_Project.sharedInstance.string_concession
        let string_date = OBJ_Project.sharedInstance.string_limitDate
        let string_ter = OBJ_Project.sharedInstance.int_term.description
        let string_interestRate = OBJ_Project.sharedInstance.int_interestRate.description
        let oneInstallment = "true"
        let string_image = ""//imageEncode(image: OBJ_Project.sharedInstance.image_farm!)
        let string_farm_longitude = OBJ_Project.sharedInstance.float_x_farm.description
        let string_farm_latitude = OBJ_Project.sharedInstance.float_y_farm.description
        let string_image_water = ""//imageEncode(image: OBJ_Project.sharedInstance.image_hole!)
        let string_water_longitude = OBJ_Project.sharedInstance.float_x_hole.description
        let string_water_latitude = OBJ_Project.sharedInstance.float_y_hole.description
        let string_amount = OBJ_Project.sharedInstance.float_totalAmount.description
        let id_cropType = OBJ_Project.sharedInstance.id_cropType.description
        
        HTTP_Requests.POST_PostProject(Title: string_title, Description: string_description, WHNumber: string_water, EndDate: string_date, Term: string_ter, InterestRate: string_interestRate, Oneinstallment: oneInstallment, FarmImage: string_image, FarmLat: string_farm_latitude, FarmLong: string_farm_longitude, WHImage: string_image_water, WHLong: string_water_longitude, WHLat: string_water_latitude, Amount: string_amount, CropTypeId: id_cropType, Categories: "", HarvestLog: "")
        
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func imageEncode(image:UIImage)->String
    {
        let media_data = UIImagePNGRepresentation(image)
        let base64_string = media_data?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64_string!
    }
    
    @IBAction func action_back(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
