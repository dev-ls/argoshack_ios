//
//  GlobalVariables.swift
//  Cass
//
//  Created by Aaron De Santiago on 10/18/16.
//  Copyright © 2016 Aaron De Santiago. All rights reserved.
//

import UIKit

class GlobalVariables: NSObject {
    static let field_back_translucent = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
    static let field_back_active = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    static let field_placeholder = UIColor(red: 118/255, green: 118/255, blue: 118/255, alpha: 1.0)
    
    static let color_darkGreen = UIColor(red: 21/255, green: 90/255, blue: 59/255, alpha: 1.0)
    
    static let view_back = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 0.9)
    
    static let appFont = UIFont.systemFont(ofSize: 16)
    static let appFontTitle = UIFont.systemFont(ofSize: 12)
}
