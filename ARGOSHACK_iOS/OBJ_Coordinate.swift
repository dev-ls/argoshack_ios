//
//  OBJ_Coordinate.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/6/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_Coordinate: NSObject {

    var longitude:Double
    var latitude:Double
    
    init(lat:Double,lon:Double) {
        self.latitude = lat
        self.longitude = lon
    }
    
}
