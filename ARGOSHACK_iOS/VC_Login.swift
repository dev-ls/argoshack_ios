//
//  VC_Login.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_Login: UIViewController {
    
    @IBOutlet weak var constraint_heightLoginForm: NSLayoutConstraint!
    @IBOutlet weak var constraint_bottom: NSLayoutConstraint!
    @IBOutlet weak var txt_username: NCTextField!
    @IBOutlet weak var txt_password: NCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        constraint_heightLoginForm.constant = 0
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Keyboard methods
    
    func keyboardWillShow(notification:NSNotification)
    {
        self.keyboardWillShow(notification: notification, constraint: constraint_bottom, constant: 0, positive: true)
    }
    func keyboardWillHide(notification:NSNotification)
    {
        self.keyboardWillHide(notification: notification, constraint: constraint_bottom)
    }
    
    // MARK: - Start app interaction
    
    @IBAction func action_showLoginForm(_ sender: AnyObject) {
        constraint_heightLoginForm.constant = 200
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func action_login(_ sender: AnyObject) {
        //let login = HTTP_Requests.get_Login(Email: txt_username.text!, Password: txt_password.text!)
        let vc_dash = self.storyboard?.instantiateViewController(withIdentifier: "VC_Dashboard") as! VC_Dashboard
        self.navigationController?.pushViewController(vc_dash, animated: true)
    }
}
