//
//  VC_Dashboard.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_Dashboard: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // API Data
    
    var array_projects = [OBJ_ProjectFromList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        array_projects = HTTP_Requests.get_ListProjects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CC_Project", for: indexPath as IndexPath) as! CC_Project
        cell.init_cell(obj_project: array_projects[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width - 20)/2
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc_detail = self.storyboard?.instantiateViewController(withIdentifier: "VC_DashboardDetail") as! VC_DashboardDetail
        vc_detail.obj_project = array_projects[indexPath.item]
        self.navigationController?.pushViewController(vc_detail, animated: true)
    }
}
