//
//  NCFormatters.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class NCFormatters: NSObject {
    
    class func numberToCurrency(_ input:Float) -> String
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = "$"
        
        return formatter.string(from: NSNumber(value: input))!
    }
    
    class func currencyToNumber(_ input:String) -> Float
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = "$"
        
        return Float(formatter.number(from: input)!)
    }
    
    class func dateToString(_ date:Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd - HH:mm"
        return formatter.string(from: date)
    }

}
