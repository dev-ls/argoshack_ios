//
//  TC_Concepto.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class TC_Concepto: UITableViewCell {

    
    @IBOutlet weak var txt_categoria: UILabel!
    @IBOutlet weak var txt_descripcion: UILabel!
    @IBOutlet weak var txt_monto: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config_Cell(categoria:String,descripcion:String,monto:String){
        
        txt_categoria.text = categoria
        txt_descripcion.text = descripcion
        txt_monto.text = monto
    }

}
