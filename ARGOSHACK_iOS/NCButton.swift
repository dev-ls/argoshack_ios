//
//  NCButton.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class NCButton: UIButton {
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
    }
}
