//
//  VC_Historial.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit



class VC_Historial: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var txt_fecha: UITextField!
    @IBOutlet weak var txt_cultivo: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let a_cultivos = HTTP_Requests.get_ListCropTypes()
    var a_historicos = [OBJ_HIstorico]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VC_Concept.DismissKeyboard))
        view.addGestureRecognizer(tap)
        
        tableView.delegate = self
        tableView.dataSource = self

        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        txt_fecha.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        
        print(a_cultivos.count)
    }
    
    //
    //TableView
    //
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return a_historicos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let historico = a_historicos[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier:"Cell_Cultivo", for: indexPath) as! TC_Historico
            cell.config_cell(fecha:historico.stringdate,cultivo:historico.cultivo)
            return cell
    }

    
    @IBAction func TUI_Agregar(_ sender: Any) {
        
        a_historicos.append(OBJ_HIstorico(Date: Date() as NSDate, Stringdate: txt_fecha.text!, Cultivo: txt_cultivo.text!))
        txt_fecha.text = ""
        txt_cultivo.text = ""
        tableView.reloadData()
    }
    
    

    //
    //Picker
    //
    
    //PICKER METHODS
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return a_cultivos.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return a_cultivos[row].nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txt_cultivo.text = a_cultivos[row].nombre
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func DismissKeyboard(){
        view.endEditing(true)
        self.view.removeFromSuperview()
    }
    
    
    func datePickerValueChanged(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        txt_fecha.text = dateFormatter.string(from: sender.date)
    }

}
