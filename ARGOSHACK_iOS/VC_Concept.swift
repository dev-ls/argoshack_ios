//
//  VC_Concept.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

protocol VC_ConceptDelegate{
    func append_Concept(concepto:OBJ_Conceptos)
}


class VC_Concept: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    let a_categoria = ["Capital de trabajo","Maquinaria y equipo","Semilla","Fertilizante"]
    
    
    @IBOutlet weak var txt_Categoria: UITextField!
    @IBOutlet weak var txt_Monto: UITextField!
    @IBOutlet weak var txt_Descripcion: UITextView!
    
    var delegate: VC_ConceptDelegate?
    var d_row = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VC_Concept.DismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        txt_Categoria.inputView = pickerView
        
        txt_Descripcion.text = "Descripcion del proyecto"
        txt_Descripcion.textColor = UIColor.lightGray
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //
    //Picker
    //
    
    //PICKER METHODS
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return a_categoria.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return a_categoria[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        d_row = row
        txt_Categoria.text = a_categoria[row]
        
    }

    
    @IBAction func TUI_Generate(_ sender: Any) {
        
        let Ccat = txt_Categoria.text
        let categoria = OBJ_Categoria(Nombre: Ccat!, Id: d_row.description)
        let Cmonto = Int(txt_Monto.text!)
        let Cdesc = txt_Descripcion.text
        
        let concepto = OBJ_Conceptos(Categoria: categoria, Descripcion: Cdesc!, Monto: Cmonto!)
        
        if let del = delegate {
            del.append_Concept(concepto: concepto)
        }
        self.view.removeFromSuperview()
        
    }
    
    
    //
    //TextField
    //
    
    func textViewDidBeginEditing(textView: UITextView) {
        
    }
    

    
    //KEYBOARD METHODS
    
    //Controladores del teclado
    func DismissKeyboard(){
        view.endEditing(true)
        self.view.removeFromSuperview()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
