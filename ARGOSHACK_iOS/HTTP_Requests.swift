//
//  HTTP_Requests.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import Foundation
import UIKit

let weburl = "http://api.argoshack.xyz/api/"

public class HTTP_Requests {
    
    
    class func get_CropTypes()->Void
    {

        let url = NSURL(string: weburl + "test/ListCropTypes")
        let request = NSMutableURLRequest(url: url! as URL)
        var response: URLResponse?
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
        } catch let error as NSError {
            print(error)
            data = nil
        }
        let serverstatuscode = self.get_Serverresponse(request: request)
        print(" email The server status is:" + serverstatuscode.description)
        if serverstatuscode == 200{
            let json = JSON(data:data! as Data)
            print(json)

        }
        if serverstatuscode == 404||serverstatuscode == 500{
            print("not found")
        }

    }
    
    class func get_ListCropTypes()->[OBJ_Categoria]{
        var a_crops = [OBJ_Categoria]()
        
        let url = NSURL(string: weburl + "ListCropTypes")
        let request = NSMutableURLRequest(url: url! as URL)
        var response: URLResponse?
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
        } catch let error as NSError {
            print(error)
            data = nil
        }
        let serverstatuscode = self.get_Serverresponse(request: request)
        print(" email The server status is:" + serverstatuscode.description)
        if serverstatuscode == 200{
            let json = JSON(data:data! as Data)
            print(json)
            for (_, JSON_Items): (String, JSON) in json["Items"]
            {
                let string_name = JSON_Items["Name"].stringValue
                let id_cropId = JSON_Items["CropTypeId"].intValue
                
                let category_crop = OBJ_Categoria(Nombre: string_name, Id: id_cropId.description)
                a_crops.append(category_crop)
            }
        }
        if serverstatuscode == 404||serverstatuscode == 500{
            print("not found")
        }
        return a_crops
    }
    
    
    class func get_Login(Email:String,Password:String)->Bool
    {
        let url = NSURL(string: weburl + "Login?Email=\(Email)&Password=\(Password)")
        let request = NSMutableURLRequest(url: url! as URL)
        
        var response: URLResponse?
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
        } catch let error as NSError {
            print(error)
            data = nil
        }
        let serverstatuscode = self.get_Serverresponse(request: request)
        print(" email The server status is:" + serverstatuscode.description)
        if serverstatuscode == 200{
            let json = JSON(data:data! as Data)
            print(json)
            return true
        }
        if serverstatuscode == 404||serverstatuscode == 500{
            print("not found")
        }
        return false
    }
    
    class func get_ListProjects()->[OBJ_ProjectFromList]
    {
    
        var array_projects = [OBJ_ProjectFromList]()
        
        let url = NSURL(string: weburl + "ListProjects")
        let request = NSMutableURLRequest(url: url! as URL)
        var response: URLResponse?
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
        } catch let error as NSError {
            print(error)
            data = nil
        }
        let serverstatuscode = self.get_Serverresponse(request: request)
        print(" email The server status is:" + serverstatuscode.description)
        if serverstatuscode == 200
        {
            let json = JSON(data:data! as Data)
            print(json)
            for (_, JSON_Items): (String, JSON) in json["Items"]
            {
                let id_projectId = JSON_Items["ProjectId"].intValue
                let url_image = JSON_Items["FarmImageUrl"].stringValue
                let string_projectName = JSON_Items["Title"].stringValue
                let string_projectAuthor = ""
                let string_projectDescription = JSON_Items["Description"].stringValue
                let string_limitDate = JSON_Items["EndDate"].stringValue
                let string_cropName = JSON_Items["CropType"]["Name"].stringValue
                let int_cropRisk = JSON_Items["CropType"]["RiskIndex"].intValue
                
                var category : [OBJ_Conceptos] = []
                let concept_id = OBJ_Categoria(Nombre: "", Id: "")
                let concept_name = "Semilla"
                let concept_amount = JSON_Items["Categories"][0]["Amount"].intValue
                let concept = OBJ_Conceptos(Categoria: concept_id, Descripcion: concept_name, Monto: concept_amount)
                category.append(concept)
                
                let float_projectAmount = JSON_Items["Amount"].floatValue
                let int_projectedPercentage = JSON_Items[""].intValue
                let int_term = JSON_Items["Term"].intValue
                let int_interesRate = JSON_Items["InterestRate"].intValue
                
                let bool_oneInstallment = JSON_Items["OneInstallment"].boolValue
                
                let obj_project = OBJ_ProjectFromList(id_project: id_projectId, string_projectName: string_projectName, string_projectAuthor: string_projectAuthor, string_projectDescription: string_projectDescription, string_limitDate: string_limitDate, string_cropName: string_cropName, int_cropRisk: int_cropRisk, array_projectCategory: category, float_projectAmount: float_projectAmount, int_projectCompletePercentage: int_projectedPercentage, int_term: int_term, int_interestRate: int_interesRate, bool_oneInstallment: bool_oneInstallment)
                obj_project.url_image = url_image
                
                array_projects.append(obj_project)
            }
        }
        if serverstatuscode == 404||serverstatuscode == 500{
            print("not found")
        }
        return array_projects
    }
    
    class func get_Project(ProjectId:String){
    
        let url = NSURL(string: weburl + "api/GetProject?ProjectId=\(ProjectId)")
        let request = NSMutableURLRequest(url: url! as URL)
        var response: URLResponse?
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
        } catch let error as NSError {
            print(error)
            data = nil
        }
        let serverstatuscode = self.get_Serverresponse(request: request)
        print(" email The server status is:" + serverstatuscode.description)
        if serverstatuscode == 200{
            let json = JSON(data:data! as Data)
            print(json)
            
        }
        if serverstatuscode == 404||serverstatuscode == 500{
            print("not found")
        }

    
    }
    
    class func post_signup(Name:String,Mobile:String,Email:String,Password:String){
    
        let url = NSURL(string:  weburl + "api/SignUp")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let postString = "{\"Name\":\"\(Name)\",\"Mobile\":\"\(Mobile)\",\"Email\":\"\(Email)\",\"Password\":\"\(Password)\"}"
        
        let data = postString.data(using: String.Encoding.utf8, allowLossyConversion:true)!
        request.httpBody = data
        
        var response: URLResponse?
        var data_response: NSData?
        do {
            data_response = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
            let json = JSON(data:data_response! as Data)
            print(json)
        } catch _ as NSError {
            data_response = nil
        }
    
    }
    
    class func POST_PostProject(Title:String,Description:String,WHNumber:String,EndDate:String,Term:String,InterestRate:String,Oneinstallment:String,FarmImage:String,FarmLat:String,FarmLong:String,WHImage:String,WHLong:String,WHLat:String,Amount:String,CropTypeId:String,Categories:String,HarvestLog:String){
    
        let url = NSURL(string:  weburl + "PostProject")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let postString = "{\"Title\":\"\(Title)\",\"Description\":\"\(Description)\",\"WaterholeConcessionNumber\":\"\(WHNumber)\",\"Term\":\"\(Term)\",\"InterestRate\":\"\(InterestRate)\",\"FarmGpsLatitude\":\"\(FarmLat)\",\"FarmGpsLongitude\":\"\(FarmLong)\",\"WaterholeGpsLatitude\":\"\(WHLat)\",\"WaterholeGpsLongitude\":\"\(WHLong)\"\"Amount\":\"\(Amount)\",\"CropTypeId\":\"\(CropTypeId)\"}"
        
        print(postString)
        
        // ,\"Categories\":\"\(Categories)\",\"HarvestLogs\":\"\(HarvestLog)\",\"WaterholeImageBase64\":\"\(WHImage)\"\"FarmImageBase64\":\"\(FarmImage)\", ,\"OneInstallment\":\"\(Oneinstallment)\" ,\"EndDate\":\"\(EndDate)\"
        
        let data = postString.data(using: String.Encoding.utf8, allowLossyConversion:true)!
        request.httpBody = data
        
        var response: URLResponse?
        var data_response: NSData?
        do {
            data_response = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
            let json = JSON(data:data_response! as Data)
            print(json)
        } catch _ as NSError {
            data_response = nil
        }
        
    }
    
    class func POST_Invest(Projectid:String,UserId:String,Amount:String){
    
        let url = NSURL(string:  weburl + "api/Invest")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let postString = "{\"ProjectId\":\"\(Projectid)\",\"UserId\":\"\(UserId)\",\"Amount\":\"\(Amount)\"}"
        
        let data = postString.data(using: String.Encoding.utf8, allowLossyConversion:true)!
        request.httpBody = data
        
        var response: URLResponse?
        var data_response: NSData?
        do {
            data_response = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
            let json = JSON(data:data_response! as Data)
            print(json)
        } catch _ as NSError {
            data_response = nil
        }
    
    }
    
    
    ///
    //Server Response
    ///
    
    class func get_Serverresponse(request:NSURLRequest)->Int{
        var response: URLResponse?
        do {
            _ = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        } catch _ as NSError {
        }
        if let httpResponse = response as? HTTPURLResponse {
            //200 ok
            //404 not found
            //504
            return httpResponse.statusCode
        }
        return 0
    }
    

}
