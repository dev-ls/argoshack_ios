//
//  OBJ_FarmerProfile.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/6/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_FarmerProfile: NSObject {

    var profileId:String
    var LegalName:String
    var BankAccountHolderName:String
    var BankingInstitution:String
    var BankAccountnum:String
    var CLABE:String
    var Active:String
    
    init(id:String,name:String,accountholdername:String,bankname:String,accountnum:String,clabe:String,active:String) {
        self.profileId = id
        self.LegalName = name
        self.BankAccountHolderName = accountholdername
        self.BankingInstitution = bankname
        self.BankAccountnum = accountnum
        self.CLABE = clabe
        self.Active = active
    }
}
