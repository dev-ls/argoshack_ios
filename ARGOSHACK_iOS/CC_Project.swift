//
//  CC_Project.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class CC_Project: UICollectionViewCell {
    
    // Outlets
    
    @IBOutlet weak var img_mainImage: UIImageView!
    @IBOutlet weak var lbl_projectName: UILabel!
    @IBOutlet weak var lbl_limitDate: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    @IBOutlet weak var lbl_projectDescription: UILabel!
    @IBOutlet weak var constraint_percentage: NSLayoutConstraint!
    
    var obj_project : OBJ_ProjectFromList!
    
    func init_cell(obj_project:OBJ_ProjectFromList)
    {
        self.obj_project = obj_project
        
        if let url = NSURL(string: obj_project.url_image) {
            if let data = NSData(contentsOf: url as URL) {
                img_mainImage.image = UIImage(data: data as Data)
            }        
        }
        
        lbl_projectName.text = obj_project.string_projectName
        lbl_limitDate.text = obj_project.string_limitDate
        lbl_amount.text = NCFormatters.numberToCurrency(obj_project.float_projectAmount) + " - " + obj_project.int_projectCompletePercentage.description + "%"
        lbl_projectDescription.text = obj_project.string_projectDescription
        calculatePercentage()
    }
    
    func calculatePercentage()
    {
        let int_percentage = obj_project.int_projectCompletePercentage
        
        let float_widthPercentage = Float(self.frame.width - 16) * (Float(int_percentage)/100.0)
        constraint_percentage.constant = CGFloat(float_widthPercentage)
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
}
