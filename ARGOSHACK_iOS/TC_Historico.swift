//
//  TC_Historico.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class TC_Historico: UITableViewCell {

    @IBOutlet weak var lbl_fecha: UILabel!
    @IBOutlet weak var lbl_cultivo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config_cell(fecha:String,cultivo:String){
        lbl_fecha.text = fecha
        lbl_cultivo.text = cultivo
    
    }
    
    
}
