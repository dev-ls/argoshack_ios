//
//  VC_DashboardDetail.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class VC_DashboardDetail: UIViewController {

    // Previous controller variables
    
    var obj_project : OBJ_ProjectFromList = OBJ_ProjectFromList()
    
    // Outlets
    
    @IBOutlet weak var img_mainProjectImage: UIImageView!
    @IBOutlet weak var lbl_projectName: UILabel!
    @IBOutlet weak var lbl_projectAuthor: UILabel!
    @IBOutlet weak var lbl_limitDate: UILabel!
    @IBOutlet weak var lbl_cropType: UILabel!
    @IBOutlet weak var lbl_cropRisk: UILabel!
    @IBOutlet weak var lbl_categoryOne: UILabel!
    @IBOutlet weak var lbl_categoryTwo: UILabel!
    @IBOutlet weak var lbl_categoryThree: UILabel!
    @IBOutlet weak var lbl_categoryFour: UILabel!
    @IBOutlet weak var lbl_projectDescription: UILabel!
    @IBOutlet weak var lbl_amount: NCButton!
    @IBOutlet weak var lbl_percentageComplete: UILabel!
    @IBOutlet weak var constraint_percentageWidth: NSLayoutConstraint!
    @IBOutlet weak var lbl_term: UILabel!
    @IBOutlet weak var lbl_onePayment: UILabel!
    @IBOutlet weak var lbl_interestRate: UILabel!
    @IBOutlet weak var lbl_colorRisk: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup of the view
        
        if let url = NSURL(string: (obj_project.url_image)) {
            if let data = NSData(contentsOf: url as URL) {
                img_mainProjectImage.image = UIImage(data: data as Data)
            }
        }
        
        lbl_projectName.text = obj_project.string_projectName
        lbl_projectAuthor.text = obj_project.string_projectAuthor
        lbl_limitDate.text = obj_project.string_limitDate
        
        lbl_cropType.text = obj_project.string_cropName + " - "
        lbl_cropRisk.text = obj_project.int_cropRisk.description
        
        if obj_project.int_cropRisk < 33
        {
            lbl_colorRisk.backgroundColor = UIColor.green
        }
        else if obj_project.int_cropRisk < 66
        {
            lbl_colorRisk.backgroundColor = UIColor.yellow
        }
        else
        {
            lbl_colorRisk.backgroundColor = UIColor.red
        }
        
        if obj_project.array_projectCategory.count == 4
        {
            lbl_categoryOne.text = obj_project.array_projectCategory[0].categoria.nombre + " - " + NCFormatters.numberToCurrency(Float(obj_project.array_projectCategory[0].monto))
            lbl_categoryTwo.text = obj_project.array_projectCategory[1].categoria.nombre + " - " + NCFormatters.numberToCurrency(Float(obj_project.array_projectCategory[1].monto))
            lbl_categoryThree.text = obj_project.array_projectCategory[2].categoria.nombre + " - " + NCFormatters.numberToCurrency(Float(obj_project.array_projectCategory[2].monto))
            lbl_categoryFour.text = obj_project.array_projectCategory[3].categoria.nombre + " - " + NCFormatters.numberToCurrency(Float(obj_project.array_projectCategory[3].monto))
        }
        
        lbl_projectDescription.text = obj_project.string_projectDescription
        lbl_amount.setTitle(NCFormatters.numberToCurrency(obj_project.float_projectAmount), for: .normal)
        lbl_percentageComplete.text = obj_project.int_projectCompletePercentage.description + "% Completado"
        
        constraint_percentageWidth.constant = CGFloat(Float(self.view.frame.width - 16) * Float(obj_project.int_projectCompletePercentage)/100.0)
        self.view.layoutIfNeeded()
        
        lbl_term.text = obj_project.int_term.description + " Meses"
        if obj_project.bool_oneInstallment
        {
            lbl_onePayment.text = "Un pago al final"
        }
        else
        {
            lbl_onePayment.text = ""
        }
        
        lbl_interestRate.text = obj_project.int_interestRate.description + "%"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navegacion
    
    @IBAction func action_investInProject(_ sender: AnyObject) {
        let vc_invest = self.storyboard?.instantiateViewController(withIdentifier: "VC_DashboardInvest") as! VC_DashboardInvest
        vc_invest.obj_project = self.obj_project
        self.navigationController?.pushViewController(vc_invest, animated: true)
        
    }
    @IBAction func action_back(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
