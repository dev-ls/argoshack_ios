//
//  OBJ_ProjectFromList.swift
//  ARGOSHACK_iOS
//
//  Created by Aaron De Santiago on 11/5/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_ProjectFromList: NSObject {
    
    var id_projectId : Int = 0
    
    var url_image : String = ""
    
    var string_projectName : String = ""
    var string_projectAuthor : String = ""
    var string_projectDescription : String = ""
    
    var string_limitDate : String = ""
    var string_cropName : String = ""
    var int_cropRisk : Int = 0
    
    var array_projectCategory : [OBJ_Conceptos] = []
    
    var float_projectAmount : Float = 0.0
    var int_projectCompletePercentage : Int = 0
    
    var int_term : Int = 0
    var int_interestRate : Int = 0
    var bool_oneInstallment : Bool = false
    
    // Empty initialization
    
    override init() {}
    
    // Standard object initialization
    
    init(id_project:Int, string_projectName:String, string_projectAuthor:String, string_projectDescription:String, string_limitDate:String, string_cropName:String, int_cropRisk:Int, array_projectCategory:[OBJ_Conceptos], float_projectAmount:Float, int_projectCompletePercentage:Int, int_term:Int, int_interestRate:Int, bool_oneInstallment:Bool) {
        
        self.id_projectId = id_project
        self.string_projectName = string_projectName
        self.string_projectAuthor = string_projectAuthor
        self.string_limitDate = string_limitDate
        self.string_cropName = string_cropName
        self.int_cropRisk = int_cropRisk
        self.array_projectCategory = array_projectCategory
        self.float_projectAmount = float_projectAmount
        self.int_projectCompletePercentage = int_projectCompletePercentage
        self.int_term = int_term
        self.int_interestRate = int_interestRate
        self.bool_oneInstallment = bool_oneInstallment
    }

}
