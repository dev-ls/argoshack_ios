//
//  OBJ_BackerProfile.swift
//  ARGOSHACK_iOS
//
//  Created by Victor Rosas on 11/6/16.
//  Copyright © 2016 Victor Rosas. All rights reserved.
//

import UIKit

class OBJ_BackerProfile: NSObject {

    var profileId:String
    var LegalName:String
    var Active: Bool
    
    init(id:String,name:String,active:Bool) {
        self.profileId = id
        self.LegalName = name
        self.Active = active
    }
    
    
}
